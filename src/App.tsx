import React from "react";
import SubmitBox from "./components/submitbox";
import UserTable from "./components/usertable";
import About from "./components/about";
import Countdown from "./components/countdown";

export type userEntry = {
  username: string;
  last_entry: string;
  streak: number;
  frozen: boolean;
};

const App = () => {
  const [entries, setEntries] = React.useState<userEntry[]>([]);

  const getData = () => {
    //fetch("http://localhost:5000/leaderboard", {
    fetch("https://morningcrew-server.vercel.app/leaderboard", {
      method: "GET",
      headers: {},
    })
      .then(res => res.json())
      .then((data: userEntry[]) =>
        setEntries(
          data.sort((a, b) => {
            if (a.frozen && !b.frozen) return 1;
            if (b.frozen && !a.frozen) return -1;
            return b.streak - a.streak;
          })
        )
      )
      .catch(err => console.log(err));
  };

  React.useEffect(() => {
    getData();
  }, []);

  return (
    <main>
      <About />
      <h1>Morning Crew Roll Call</h1>
      <Countdown />
      <SubmitBox onSubmit={getData} />
      <UserTable userEntries={entries} />
    </main>
  );
};

export default App;

import React from "react";
import moment from "moment-timezone";

const useCphTime = () => {
  const { weekday, currentDate, currentTime } = React.useMemo(() => {
    const cph_time = moment().tz("Europe/Copenhagen").format();
    const regex = /(20\d\d)-(\d\d)-(\d\d)T(\d\d:\d\d:\d\d).*/g;
    const matches = Array.from(cph_time.matchAll(regex));
    const [full, year, month, date, time] = matches[0];
    return {
      weekday: moment(`${year}-${month}-${date}`).isoWeekday(),
      currentDate: `${date}.${month}.${year}`,
      currentTime: time,
    };
  }, []);

  return { weekday, currentDate, currentTime };
};

export default useCphTime;

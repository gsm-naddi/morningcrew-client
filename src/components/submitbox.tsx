import React from "react";
import useCphTime from "../hooks/useCphTime";

interface Props {
  onSubmit: () => void;
}

const SubmitBox: React.FC<Props> = ({ onSubmit }) => {
  React.useEffect(() => {
    const f = async () => {
      const existing = window.localStorage.getItem("ip");
      if (existing) return;

      const ip = await fetch("https://api.ipify.org?format=json")
        .then(res => res.json())
        .then(data => {
          return data.ip;
        });
      window.localStorage.setItem("ip", ip);
    };
    f();
  }, []);
  const inputRef = React.useRef<HTMLInputElement>(null);
  const [error, setError] = React.useState<string>();
  const [submittext, setSubmittext] = React.useState<string>();
  const { weekday, currentTime } = useCphTime();
  const isWeekend = React.useMemo(
    () => weekday === 6 || weekday === 7,
    [weekday]
  );
  const validForSubmission = React.useMemo(
    () => currentTime < "08:05:00" && currentTime > "05:55:00",
    [currentTime]
  );

  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      handleClick();
    }
  };

  const handleClick = async () => {
    const username = inputRef.current?.value.trim();
    if (username && username.length) {
      window.localStorage.setItem("username", username);
      const ip = window.localStorage.getItem("ip");
      fetch(
        //`http://localhost:5000/submit?username=${username}&ip=${ip}`,
        `https://morningcrew-server.vercel.app/submit?username=${username}&ip=${ip}`,
        {
          method: "GET",
        }
      )
        .then(res => {
          if (!res.ok) {
            return res.json();
          } else {
            onSubmit();
            setSubmittext("You're in!");
            setError(undefined);
            setTimeout(() => {
              setSubmittext(undefined);
            }, 5000);
          }
        })
        .then(data => {
          if (data) {
            setError(data.message);
            setTimeout(() => {
              setError(undefined);
            }, 5000);
          }
        });
    } else {
      setError("Please write a username.");
      setTimeout(() => {
        setError(undefined);
      }, 5000);
    }
  };

  if (false) {
    return (
      <div className="submitbox">
        <h2>Morning crew is down right now :(</h2>
        <p>
          Okay everyone it seems we have troll on the morning crew today.
          Because some names have been entered incorrectly and we can see it
          happened within seconds of each other. To however who did this: please
          stop, don&apos;t go around and trying to ruin the fun for everyone
          else on here. Work on your own streak - do&apos;t try and sabotage
          everyone else&apos;s.
        </p>
        <p>
          We are fixing the error manually, but it will take some time. Please
          write "here" in the <a href="">forum topic</a> to register.
        </p>
      </div>
    );
  }

  if (validForSubmission)
    return (
      <div className="submitbox">
        {isWeekend ? (
          <>
            <h2>Good morning!</h2>
            <span>
              It's the weekend, so the roll call is on a break. You can't sign
              up today. 💤
            </span>
          </>
        ) : (
          <>
            <h2>Good morning &ndash; join the roll call</h2>
            <p className="smaller">
              Even though it is now possible to maintain your streak without
              logging in on gsm, we still encourage you to take part in the{" "}
              <a
                target="_blank"
                rel="noreferrer"
                href="https://gosupermodel.com/community/forum_room.jsp?id=11"
              >
                morning crew forum topic
              </a>{" "}
              &ndash; even if it&apos;s just a quick "good morning". The morning
              crew is more than just having a streak. It is a community for all
              of us early birds where we can connect with each other. So if you
              have the time, please come and have a morning chat with us, so we
              can keep the community thriving. ❤️
            </p>
            <p className="small mb">The textfield is case sensitive.</p>
            <div className="row">
              <input
                type="text"
                ref={inputRef}
                defaultValue={
                  window.localStorage.getItem("username") || undefined
                }
                placeholder="Write your username here"
                onKeyUp={handleKeyPress}
              />
              <button onClick={handleClick}>submit</button>
              <span className="success">
                {submittext && `✓  ${submittext}`}
              </span>
            </div>
            <div className="error">{error}</div>
          </>
        )}
      </div>
    );
  else return <></>;
};

export default SubmitBox;

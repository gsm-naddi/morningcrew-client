import React from "react";
import { DaysToSmiley, DaysToSmiley_christmas } from "./legend";
import { userEntry } from "../App";
import useCphTime from "../hooks/useCphTime";

interface Props {
  entry: userEntry;
}

const getHighestValid = (n: number) => {
  const keys = [500, 200, 150, 125, 100, 50, 25, 10, 5, 1];
  for (var i = 0; i < keys.length; i++) {
    if (n >= keys[i]) return keys[i];
  }
  return 0;
};

const getEmojis = (streak: number, legendmap: Map<number, string>) => {
  var str = "";
  while (true) {
    const key = getHighestValid(streak);
    if (key === 0) return str;
    str = str + legendmap.get(key);
    streak = streak - key;
  }
};

const getRandomColor = () => {
  const colors = ["v1", "v2", "v3", "v4", "v5"];
  const index = Math.floor(Math.random() * colors.length);
  return colors[index];
};

const validTime = (currentTime: string) =>
  currentTime < "08:05:00" && currentTime > "05:55:00";

const UserItem: React.FC<Props> = ({ entry }) => {
  const { currentDate, currentTime } = useCphTime();
  const legendmap: Map<number, string> = React.useMemo(
    () =>
      document.body.classList.contains("winter")
        ? DaysToSmiley_christmas
        : DaysToSmiley,
    []
  );

  return (
    <tr className="user">
      <td>
        <div className="row">
          {entry.username} {getEmojis(entry.streak, legendmap)}
        </div>
      </td>
      <td>
        {entry.last_entry === currentDate && validTime(currentTime) && (
          <div className={`label ${getRandomColor()}`}>here today!</div>
        )}
        {entry.frozen && <div className={`label frozen`}>frozen</div>}
      </td>
      <td className="streak">
        <span className="small bold">x{entry.streak}</span>
      </td>
    </tr>
  );
};

export default UserItem;

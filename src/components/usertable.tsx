import React from "react";
import Legend from "./legend";
import UserItem from "./user-item";
import Spinner from "./spinner";
import { userEntry } from "../App";

interface Props {
  userEntries: userEntry[];
}

const UserTable: React.FC<Props> = ({ userEntries }) => {
  const [searchString, setSearchString] = React.useState("");

  return (
    <>
      <input
        className="search"
        type="text"
        placeholder="Search"
        onChange={e => setSearchString(e.target.value)}
      />
      <Legend />
      <table className="container">
        <tbody>
          {userEntries.length === 0 && (
            <tr>
              <td className="full">
                <Spinner />
              </td>
            </tr>
          )}
          {userEntries
            .filter(e =>
              e.username.toLowerCase().includes(searchString.toLowerCase())
            )
            .map(e => (
              <UserItem key={e.username} entry={e} />
            ))}
        </tbody>
      </table>
    </>
  );
};

export default UserTable;

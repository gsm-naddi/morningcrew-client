import React from "react";

const About = () => {
  const [open, setOpen] = React.useState(false);

  const handleClick = () => setOpen(true);
  const onClose = () => setOpen(false);

  return (
    <>
      <button className="about" onClick={handleClick} name="About">
        About
      </button>

      <>
        {open && (
          <>
            <div className="background" onClick={onClose} />
            <dialog open={open}>
              <div className="row">
                <h2>About</h2>
                <button onClick={onClose}>✖</button>
              </div>
              <p>
                This is the morning crew roll call for users on goSupermodel.
                The roll call is usually maintained by user{" "}
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://gosupermodel.com/profiles/?id=186322"
                >
                  Daiyuna
                </a>
                , but as she needs a break, we have automated it.
              </p>
              <p>
                The roll call is now hosted by{" "}
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://gosupermodel.com/profiles/?id=169773"
                >
                  Aquilegia
                </a>{" "}
                and{" "}
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://gosupermodel.com/profiles/?id=146605"
                >
                  Fractal
                </a>{" "}
                . Please remember to keep the forum topic alive and say good
                morning to each other even though the roll call is automated
                &lt;3
              </p>
              __________
              <p className="small">
                This site is made by user{" "}
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://gosupermodel.com/profiles/?id=3379"
                >
                  Naddi
                </a>{" "}
                with design inspiration from the{" "}
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://www.nightcrew.app/"
                >
                  Night crew website
                </a>
                .
              </p>
            </dialog>
          </>
        )}
      </>
    </>
  );
};

export default About;

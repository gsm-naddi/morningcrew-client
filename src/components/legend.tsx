import React from "react";

export const DaysToSmiley = new Map([
  [1, "😊"],
  [5, "🥰"],
  [10, "💛"],
  [25, "🧡"],
  [50, "❤️"],
  [75, "🌿"],
  [100, "🌷"],
  [125, "🌼"],
  [150, "🌻"],
  [200, "☀️"],
]);

export const DaysToSmiley_christmas = new Map([
  [1, "❄️"],
  [5, "⛄"],
  [10, "🥁"],
  [25, "❤️"],
  [50, "🔔"],
  [75, "🎀"],
  [100, "🎺"],
  [125, "🎁"],
  [150, "🎄"],
  [200, "🌟"],
]);

const Legend: React.FC = () => {
  const legendmap: Map<number, string> = React.useMemo(
    () =>
      document.body.classList.contains("winter")
        ? DaysToSmiley_christmas
        : DaysToSmiley,
    []
  );
  return (
    <div className="wrap small center">
      {[1, 5, 10, 25, 50, 75, 100, 125, 150, 200].map(n => (
        <span key={n}>
          {legendmap.get(n)} {n} days
        </span>
      ))}
    </div>
  );
};

export default Legend;

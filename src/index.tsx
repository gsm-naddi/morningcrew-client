import React from "react";
import ReactDOM from "react-dom/client";
import "./index.scss";
import App from "./App";
import "@fontsource/abril-fatface";
import Snowfall from "react-snowfall";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
const snowflake1 = document.createElement("img");
snowflake1.src = "/snow/snow1.png";
const snowflake2 = document.createElement("img");
snowflake2.src = "/snow/snow2.png";
const snowflake3 = document.createElement("img");
snowflake3.src = "/snow/snow3.png";
const snowflake4 = document.createElement("img");
snowflake4.src = "/snow/snow4.png";
const snowflake5 = document.createElement("img");
snowflake5.src = "/snow/snow5.png";

const images = [snowflake1, snowflake2, snowflake3, snowflake4, snowflake5];

root.render(
  <React.StrictMode>
    <Snowfall
      snowflakeCount={100}
      images={images as unknown as CanvasImageSource[]}
      wind={[-1, 0.1]}
      radius={[3, 10]}
      speed={[0.1, 0.5]}
    />
    <App />
  </React.StrictMode>
);
